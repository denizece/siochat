FROM mhart/alpine-node:latest

ENV USE_HOST_NAMES=true
ENV MULTICAST_ADDRESS=239.10.21.121

WORKDIR /src
ADD . .

RUN npm install
